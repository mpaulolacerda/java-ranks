package net.magmaland.rank.minecraft.commands;

import me.saiintbrisson.minecraft.command.annotation.Command;
import me.saiintbrisson.minecraft.command.command.Context;
import me.saiintbrisson.minecraft.command.target.CommandTarget;
import net.magmaland.rank.api.RankUserAPI;
import net.magmaland.rank.entity.error.exceptions.InvalidPlayerException;
import org.bukkit.entity.Player;

public class RankCommand {

    private final RankUserAPI rankUserAPI = RankUserAPI.getInstance();

    @Command(name = "rank", target = CommandTarget.PLAYER)
    public void handleCommand(Context<Player> context) {
        try {
            context.sendMessage("§aSeu rank atual: §f" + rankUserAPI.getRankUser(
                    context.getSender()
            ).getCurrentRank().getRankIdentifier().replace('&', '§'));
        } catch (InvalidPlayerException exception) {
            exception.printStackTrace();
        }
    }

}
