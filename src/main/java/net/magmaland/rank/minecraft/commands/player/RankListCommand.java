package net.magmaland.rank.minecraft.commands.player;

import lombok.Getter;
import me.saiintbrisson.minecraft.command.annotation.Command;
import me.saiintbrisson.minecraft.command.command.Context;
import me.saiintbrisson.minecraft.command.target.CommandTarget;

import net.magmaland.rank.minecraft.inventories.RankInventoryView;
import org.bukkit.entity.Player;

public class RankListCommand {

    @Getter private final RankInventoryView rankInventoryView;

    public RankListCommand(RankInventoryView rankInventoryView) {
        this.rankInventoryView = rankInventoryView;
    }

    @Command(name = "ranks", target = CommandTarget.PLAYER)
    public void handleCommand(Context<Player> context) {
        rankInventoryView.getView().showInventory(context.getSender());
    }

}
