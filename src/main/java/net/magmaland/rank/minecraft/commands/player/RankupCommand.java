package net.magmaland.rank.minecraft.commands.player;

import me.saiintbrisson.minecraft.command.annotation.Command;
import me.saiintbrisson.minecraft.command.command.Context;
import me.saiintbrisson.minecraft.command.target.CommandTarget;
import net.magmaland.motherboard.utils.cooldown.Cooldown;
import net.magmaland.motherboard.utils.cooldown.CooldownManager;
import net.magmaland.rank.api.RankUserAPI;
import net.magmaland.rank.entity.error.exceptions.InsufficientMoneyException;
import net.magmaland.rank.entity.error.exceptions.InvalidPlayerException;
import net.magmaland.rank.entity.error.exceptions.LastRankException;
import org.bukkit.Sound;
import org.bukkit.entity.Player;

import java.util.concurrent.TimeUnit;

public class RankupCommand {

    private final RankUserAPI rankUserAPI = RankUserAPI.getInstance();

    private final CooldownManager cooldownManager = CooldownManager.getInstance();

    @Command(name = "rankup", target = CommandTarget.PLAYER)
    public void handleCommand(Context<Player> context) {
        final Player player = context.getSender();
        final Cooldown playerCooldown = cooldownManager.getCooldown(player.getUniqueId(), "RANK_UP");

        if (playerCooldown != null && !playerCooldown.hasExpired()) {
            player.sendMessage("§c[ERRO] Aguarde " + playerCooldown.getRemaining(TimeUnit.SECONDS) + " segundos para upar de rank novamente.");
            return;
        }

        if (!player.hasPermission("rank.cooldown.bypass")) {
            cooldownManager.registerCooldown(player.getUniqueId(), "RANK_UP", TimeUnit.SECONDS.toMillis(5));
        }

        try {
            rankUserAPI.upgradeRank(player);
        } catch (InvalidPlayerException exception) {
            exception.process(player);
        } catch (LastRankException exception) {
            exception.process(player);
        } catch (InsufficientMoneyException exception) {
            exception.process(player);
        }
    }

}
