package net.magmaland.rank.minecraft.listeners.rank;

import lombok.Getter;
import net.magmaland.rank.entity.Rank;
import org.bukkit.event.Cancellable;
import org.bukkit.event.Event;
import org.bukkit.event.HandlerList;

public class PlayerRankUpgradeEvent extends Event implements Cancellable {

    @Getter
    private final Rank previousRank, newRank;
    private boolean isCancelled;

    private static final HandlerList HANDLER_LIST = new HandlerList();

    public PlayerRankUpgradeEvent(Rank previousRank, Rank newRank) {
        this.previousRank = previousRank;
        this.newRank = newRank;
        this.isCancelled = false;
    }

    @Override
    public boolean isCancelled() {
        return isCancelled;
    }

    @Override
    public void setCancelled(boolean cancel) {
        this.isCancelled = true;
    }

    @Override
    public HandlerList getHandlers() {
        return HANDLER_LIST;
    }

    public static HandlerList getHandlerList() {
        return HANDLER_LIST;
    }

}
