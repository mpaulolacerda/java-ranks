package net.magmaland.rank.minecraft.listeners;

import net.magmaland.rank.RankPlugin;
import net.magmaland.rank.entity.repository.RankUserRepository;
import net.magmaland.rank.entity.user.RankUser;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerJoinEvent;
import org.bukkit.event.player.PlayerQuitEvent;

import java.util.UUID;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.LinkedBlockingDeque;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;

public class PlayerConnectionListener implements Listener {

    private final RankPlugin rankPlugin = RankPlugin.getInstance();
    private final RankUserRepository rankUserRepository = rankPlugin.getDatabaseProvider()
            .getRepository(RankUserRepository.class);

    private final ExecutorService executorService = new ThreadPoolExecutor(
            2, 4, 15, TimeUnit.SECONDS,
            new LinkedBlockingDeque<>()
    );

    @EventHandler
    public void onJoin(PlayerJoinEvent event) {
        final UUID uniqueId = event.getPlayer().getUniqueId();

        executorService.submit(() -> {
            final RankUser rankUser = rankUserRepository.search(uniqueId).getResponse();

            if (rankUser != null) {
                rankPlugin.getRankUserCache().put(rankUser.getUniqueId(), rankUser);
            } else {
                RankUser def = new RankUser(uniqueId);
                rankPlugin.getRankUserCache().put(def.getUniqueId(), def);
            }
        });
    }

    @EventHandler
    public void onQuit(PlayerQuitEvent event) {
        final UUID uniqueId = event.getPlayer().getUniqueId();

        executorService.submit(() -> {
            final RankUser rankUser = rankPlugin.getRankUserCache().get(uniqueId);

            if (rankUser != null) {
                rankUserRepository.insert(rankUser);
                rankPlugin.getRankUserCache().remove(rankUser.getUniqueId());
            }
        });
    }

}
