package net.magmaland.rank.minecraft.inventories;

import lombok.Getter;
import lombok.NonNull;
import me.saiintbrisson.minecraft.paginator.PaginatedView;
import me.saiintbrisson.minecraft.paginator.PaginatedViewHolder;
import me.saiintbrisson.minecraft.view.View;
import me.saiintbrisson.minecraft.view.ViewItem;
import net.magmaland.motherboard.utils.builders.ItemBuilder;
import net.magmaland.rank.RankPlugin;
import net.magmaland.rank.entity.Rank;
import org.bukkit.Material;
import org.bukkit.plugin.Plugin;

import java.util.List;

public class RankInventoryView extends View<Rank> {

    private final static List<Rank> loadedRanks = RankPlugin.getInstance()
            .getRankController().getRanks();

    @Getter private final PaginatedView<Rank> view;

    public RankInventoryView(@NonNull Plugin owner) {
        super(owner, "Ranks Disponíveis", 6);

        view = new PaginatedView<>(owner, "Ranks Disponíveis", new String[]{
                "OOOOOOOOO",
                "OOXOXOXOO",
                "OOXOXOXOO",
                "OOXOXOXOO",
                "OOOOOOOOO",
                "OOO<O>OOO"
        }, () -> loadedRanks);

        view.setNextButton(
                new ViewItem<Rank>()
                .withItem(new ItemBuilder(2257).name("§aPróxima página").removeAttributes().build())
                .withSlot(5, 5)
                .onClick((node, player, event) ->
                    ((PaginatedViewHolder) event.getInventory().getHolder()).increasePage()
                )
        );

        view.setPreviousButton(
                new ViewItem<Rank>()
                .withItem(new ItemBuilder(2259).name("§cPágina anterior").removeAttributes().build())
                .withSlot(5, 3)
                .onClick((node, player, event) ->
                        ((PaginatedViewHolder) event.getInventory().getHolder()).decreasePage()
                )
        );

        view.addItem(new ViewItem<Rank>()
                .withItem(new ItemBuilder(Material.BARRIER).name("§cFechar inventário").build())
                .withSlot(5, 4)
                .onClick((node, player, event) ->
                        player.closeInventory()
                )
        );

    }

}
