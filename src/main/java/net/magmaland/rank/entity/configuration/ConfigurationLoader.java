package net.magmaland.rank.entity.configuration;

import lombok.Builder;
import lombok.Getter;
import lombok.RequiredArgsConstructor;
import org.bukkit.plugin.Plugin;

import java.util.List;

@Builder
public class ConfigurationLoader {

    public final Plugin plugin;
    public final List<String> LORE_RANK_ICON;
    public final String RANK_ICON_DISPLAY_NAME, SQL_HOST, SQL_USERNAME, SQL_PASSWORD, SQL_DATABASE;
    public final int SQL_PORT;;

}
