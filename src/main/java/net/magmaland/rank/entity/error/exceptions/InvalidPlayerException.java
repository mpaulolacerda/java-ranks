package net.magmaland.rank.entity.error.exceptions;

import org.bukkit.ChatColor;
import org.bukkit.entity.Player;

public class InvalidPlayerException extends Throwable {

    private final String EXCEPTION = ChatColor.RED + "[ERRO] Jogador não encontrado.";

    public void process(Player target) {
        target.sendMessage(EXCEPTION);
    }

}
