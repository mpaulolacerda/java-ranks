package net.magmaland.rank.entity.error;

import org.bukkit.ChatColor;
import org.bukkit.Sound;
import org.bukkit.entity.Player;

public abstract class ErrorThrowable extends Throwable {

    public void process(Player target, String EXCEPTION) {
        target.sendMessage(ChatColor.RED + EXCEPTION);

        target.playSound(
                target.getLocation(),
                Sound.VILLAGER_NO, 100, 1
        );
    }

}
