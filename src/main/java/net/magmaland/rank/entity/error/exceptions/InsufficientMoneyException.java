package net.magmaland.rank.entity.error.exceptions;

import lombok.RequiredArgsConstructor;
import net.magmaland.motherboard.formatter.CoinsFormatter;
import org.bukkit.ChatColor;
import org.bukkit.entity.Player;

@RequiredArgsConstructor
public class InsufficientMoneyException extends Throwable {

    private final String EXCEPTION = ChatColor.RED + "[ERRO] Você não tem coins suficientes para isso! (%s)";

    private final double cost;

    public void process(Player target) {
        target.sendMessage(String.format(
                EXCEPTION, CoinsFormatter.format(cost)
        ));
    }

}
