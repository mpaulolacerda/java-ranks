package net.magmaland.rank.entity.error.exceptions;

import org.bukkit.ChatColor;
import org.bukkit.entity.Player;

public class LastRankException extends Throwable {

    private final String EXCEPTION = ChatColor.GREEN + "Você já está no último rank.";

    public void process(Player target) {
        target.sendMessage(EXCEPTION);
    }

}
