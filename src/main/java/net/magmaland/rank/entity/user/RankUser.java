package net.magmaland.rank.entity.user;

import lombok.Data;
import lombok.SneakyThrows;
import net.magmaland.rank.RankPlugin;
import net.magmaland.rank.entity.Rank;

import java.util.UUID;

@Data
public class RankUser {

    private final UUID uniqueId;
    private Rank currentRank;

    public RankUser(UUID uniqueId, Rank currentRank) {
        this.uniqueId = uniqueId;
        this.currentRank = currentRank;
    }

    @SneakyThrows
    public RankUser(UUID uniqueId) {
        this.uniqueId = uniqueId;
        this.currentRank = RankPlugin.getInstance().getRankController().byPosition(0);
    }

}
