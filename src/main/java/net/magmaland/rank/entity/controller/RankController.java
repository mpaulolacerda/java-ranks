package net.magmaland.rank.entity.controller;

import lombok.Getter;
import net.magmaland.rank.entity.Rank;
import net.magmaland.rank.entity.error.exceptions.LastRankException;

import java.util.ArrayList;
import java.util.List;

@Getter
public class RankController {

    private final List<Rank> ranks;

    public RankController() {
        this.ranks = new ArrayList<>();
    }

    public Rank byPosition(int position) throws LastRankException {
        if (position == -1) return null;
        if ((position + 1) >= ranks.size()) throw new LastRankException();

        return ranks.get(position);
    }

    public Rank byId(String rankId) {
        for (Rank rank : ranks) {
            if (rank.getRankId().equalsIgnoreCase(rankId)) return rank;
        }

        return null;
    }

    public void insertRank(Rank rank) {
        ranks.add(rank);
    }

    public boolean empty() { return ranks.isEmpty(); }

}
