package net.magmaland.rank.entity.adapter.repository;

import lombok.SneakyThrows;
import me.saiintbrisson.db.adapter.AdapterContext;
import me.saiintbrisson.db.adapter.SqlAdapter;
import net.magmaland.rank.RankPlugin;
import net.magmaland.rank.entity.user.RankUser;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.UUID;

public class RankUserAdapter extends SqlAdapter<RankUser> {

    private final RankPlugin rankPlugin = RankPlugin.getInstance();

    @SneakyThrows
    @Override
    protected RankUser read(ResultSet resultSet, AdapterContext context) throws SQLException {
        String rawUniqueId = resultSet.getString(1);
        int userRankPosition = resultSet.getInt(2);

        return new RankUser(
                UUID.fromString(rawUniqueId),
                rankPlugin.getRankController().byPosition(userRankPosition)
        );
    }

    @Override
    public void write(RankUser rankUser, PreparedStatement statement) throws SQLException {
        statement.setString(1, rankUser.getUniqueId().toString());
        statement.setInt(2, rankUser.getCurrentRank().getPosition());

        statement.setInt(3, rankUser.getCurrentRank().getPosition()); //update
    }

}
