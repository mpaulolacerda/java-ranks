package net.magmaland.rank.entity.adapter;

import net.magmaland.motherboard.utils.builders.ItemBuilder;
import net.magmaland.motherboard.utils.builders.SkullBuilder;
import net.magmaland.rank.RankPlugin;
import net.magmaland.rank.entity.Rank;
import org.bukkit.Material;
import org.bukkit.configuration.ConfigurationSection;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.configuration.file.YamlConfiguration;
import org.bukkit.inventory.ItemStack;

import java.io.File;

public class RankFileAdapter {

    public Rank convert(File file) {
        if (file == null) return null;
        if (file.isDirectory()) return null;

        String fileIdenfitier = file.getName();
        if (!fileIdenfitier.endsWith("_rank.yml")) return null;
        FileConfiguration fileConfiguration = YamlConfiguration.loadConfiguration(file);

        String def = fileIdenfitier.toLowerCase().substring(0, fileIdenfitier.length() - 9);

        int position = fileConfiguration.getInt("position");
        if (position == -1) {
            RankPlugin.getInstance().getLogger().severe(
                    "[ERRO] A posição do rank " + def + " está incorreta. (0)"
            );

            return null;
        }

        String rankIdentifier = fileConfiguration.getString("rankIdentifier", def);
        String rankPrefix = fileConfiguration.getString("rankPrefix", "[" + def + "]");

        double cost = fileConfiguration.getDouble("cost", 0D);

        int maxLevel = fileConfiguration.getInt("max-level", 1);

        ItemStack defIcon = new ItemStack(Material.STONE); //if icon is invalid
        ConfigurationSection configurationSection = fileConfiguration.getConfigurationSection("icon");

        if (configurationSection != null) {
            Material material = Material.getMaterial(configurationSection.getString("material").toUpperCase());
            if (material == null) return null;

            int data = configurationSection.getInt("data");
            if (data < 0) return null;

            boolean isSkull = configurationSection.isBoolean("is-skull");
            String skullSkin = configurationSection.getString("skull-skin");

            defIcon = new ItemBuilder(material)
                    .durability((short) data)
                    .build();

            if (isSkull) SkullBuilder.injectSkin(defIcon, skullSkin);
        }

        return Rank.builder()
                .position(position)
                .rankIdentifier(rankIdentifier)
                .rankPrefix(rankPrefix)
                .rankId(def)
                .cost(cost)
                .icon(defIcon)
                .maxLevel(maxLevel)
                .build();
    }

}
