package net.magmaland.rank.entity.cache;

import com.google.common.cache.Cache;
import com.google.common.cache.CacheBuilder;
import net.magmaland.motherboard.memory.cache.CacheAdapter;
import net.magmaland.rank.entity.user.RankUser;

import java.util.UUID;

public class RankUserCache implements CacheAdapter<UUID, RankUser> {

    private final Cache<UUID, RankUser> rankUserCache =
            CacheBuilder.newBuilder()
                    .initialCapacity(200)
                    .concurrencyLevel(4)
                    .build();

    @Override
    public Cache<UUID, RankUser> getCache() {
        return rankUserCache;
    }

    @Override
    public RankUser get(UUID key) {
        return rankUserCache.getIfPresent(key);
    }

    @Override
    public void put(UUID key, RankUser value) {
        rankUserCache.put(key, value);
    }

    @Override
    public void remove(UUID key) {
        rankUserCache.invalidate(key);
    }

    @Override
    public void clear() {
        rankUserCache.invalidateAll();
    }

}
