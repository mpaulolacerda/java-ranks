package net.magmaland.rank.entity.queue;

import lombok.Getter;
import net.magmaland.rank.RankPlugin;
import net.magmaland.rank.entity.repository.RankUserRepository;
import net.magmaland.rank.entity.user.RankUser;

import java.util.Queue;
import java.util.UUID;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.Executors;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.TimeUnit;

public class RankUserQueue {

    private final RankPlugin rankPlugin = RankPlugin.getInstance();
    private final RankUserRepository rankUserRepository = rankPlugin
            .getDatabaseProvider().getRepository(RankUserRepository.class);

    @Getter public static RankUserQueue instance;
    private final Queue<UUID> queue;

    public RankUserQueue() {
        instance = this;
        queue = new LinkedBlockingQueue<>();
    }

    public void insertQueue(RankUser rankUser) {
        final UUID uuid = rankUser.getUniqueId();
        if (!queue.contains(uuid)) queue.add(uuid);
    }

    public void destroyQueue(RankUser rankUser) {
        final UUID uuid = rankUser.getUniqueId();
        if (queue.contains(uuid)) queue.remove(uuid);
    }

    public void startOptimizingFlush() {
        final RankUser rankUser = rankPlugin.getRankUserCache().get(queue.poll());

        if (rankUser != null) {
            CompletableFuture.runAsync(() -> rankUserRepository.insert(rankUser));
        }
    }

    public void flush() {
        while (!queue.isEmpty()) {
            rankPlugin.getLogger().warning("Salvando " + queue.size() + " rank_users.");
            startOptimizingFlush();
        }
    }

    public void start() {
        Executors.newSingleThreadScheduledExecutor().scheduleWithFixedDelay(
                this::flush, 3L, 3L,
                TimeUnit.MINUTES
        );
    }

}
