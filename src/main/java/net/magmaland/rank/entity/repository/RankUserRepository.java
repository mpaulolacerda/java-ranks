package net.magmaland.rank.entity.repository;

import me.saiintbrisson.db.Database;
import me.saiintbrisson.db.Repository;
import me.saiintbrisson.db.adapter.SqlAdapter;
import me.saiintbrisson.db.response.SqlResponse;
import me.saiintbrisson.safe.Try;
import me.saiintbrisson.util.ReportedSupplier;
import net.magmaland.rank.entity.user.RankUser;

import java.sql.ResultSet;
import java.util.UUID;

public class RankUserRepository implements Repository<RankUser, UUID> {

    private final Database database;
    private final SqlAdapter<RankUser> adapter;

    public RankUserRepository(Database database) {
        this.database = database;
        this.adapter  = database.getAdapter(RankUser.class);
    }

    @Override
    public Database getDatabase() {
        return database;
    }

    @Override
    public SqlAdapter<RankUser> getAdapter() {
        return adapter;
    }

    @Override
    public SqlResponse<RankUser> search(UUID uuid) {
        ResultSet resultSet = Try.ofSupplier(select(uuid))
                .report("Passando o rank_user " + uuid)
                .or(null);

        if (resultSet == null) return nullResponse();
        if (!Try.ofSupplier(resultSet::next).or(false)) return nullResponse();

        RankUser rankUser = Try.ofSupplier(() -> getAdapter().read(resultSet))
                .report("Recebendo o rank_user " + uuid)
                .or(null);

        return response(rankUser);
    }

    @Override
    public boolean insert(RankUser object) {
        return Try.ofSupplier(insertUser(object))
                .report("Inserindo o rankuser " + object.getUniqueId())
                .or(false);
    }

    @Override
    public boolean update(RankUser object) {
        return Try.ofSupplier(insertUser(object))
                .report("Inserindo o rankuser " + object.getUniqueId())
                .or(false);
    }

    @Override
    public boolean delete(UUID uuid) {
        return Try.of(() ->
            executeUpdate(
                    getDatabase().getStoredSql("delete_rankuser"),
                    $ -> $.setString(1, uuid.toString())
            )
        ).report("Deletando a conta " + uuid).printStackTrace().hasPassed();
    }

    @Override
    public boolean createTable() {
        return Try.of(() ->
            executeUpdate(
                    getDatabase().getStoredSql("create_rankuser_database"),
                    null
            )
        ).report("Criando a tabela rankuser").printStackTrace().hasPassed();
    }

    @Override
    public boolean dropTable() {
        return false;
    }

    private ReportedSupplier<ResultSet> select(UUID uniqueId) {
        return () -> executeQuery(
                getDatabase().getStoredSql("select_rankuser"),
                $ -> $.setString(1, uniqueId.toString())
        );
    }

    private ReportedSupplier<Boolean> insertUser(RankUser rankUser) {
        return () -> executeUpdate(
                getDatabase().getStoredSql("insert_rankuser"),
                $ -> getAdapter().write(rankUser, $)
        ) != 0;
    }

}
