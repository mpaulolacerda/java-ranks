package net.magmaland.rank.entity.wrapper.types;

import net.magmaland.rank.actionbar.ActionBar;
import net.magmaland.rank.entity.Rank;

import net.magmaland.rank.minecraft.listeners.rank.PlayerRankUpgradeEvent;
import org.bukkit.Bukkit;
import org.bukkit.entity.Player;

public class RankUpgradeWrapper {

    private static final String UPGRADE_MESSAGE = "§6O jogador §f%s §6upou para §e%s§6.";

    public static void whenUpgrade(Player player, Rank previousRank, Rank newRank) {
        Bukkit.getPluginManager().callEvent(new PlayerRankUpgradeEvent(previousRank, newRank));

        Bukkit.broadcastMessage(String.format(
                UPGRADE_MESSAGE, player.getDisplayName(),
                newRank.getRankIdentifier().replace('&', '§')
        ));

        /*Bukkit.getOnlinePlayers().forEach(players -> new ActionBar(
                String.format(
                        UPGRADE_MESSAGE, player.getDisplayName(),
                        newRank.getRankIdentifier()
                )).send(players)
        );*/
    }

}
