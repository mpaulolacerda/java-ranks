package net.magmaland.rank.entity;

import lombok.Builder;
import lombok.Data;
import lombok.Getter;
import lombok.Setter;
import me.saiintbrisson.minecraft.paginator.PaginatedItem;
import me.saiintbrisson.minecraft.paginator.PaginatedViewHolder;
import net.magmaland.rank.RankPlugin;
import net.magmaland.rank.entity.wrapper.types.RankUpgradeWrapper;
import org.bukkit.ChatColor;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

import java.util.stream.Collectors;

import static net.magmaland.rank.RankPlugin.getConfigurationLoader;
import static net.magmaland.motherboard.formatter.CoinsFormatter.format;
import static net.magmaland.economy.api.EconomyUserAPI.getInstance;

@Data @Builder
public class Rank implements PaginatedItem {

    private final String rankIdentifier, rankPrefix, rankId;
    private final int position;
    private final int maxLevel;
    private final ItemStack icon;

    private final Double cost;

    @Override
    public ItemStack toItemStack(Player viewer, PaginatedViewHolder holder) {
        ItemMeta itemMeta = icon.getItemMeta();

        itemMeta.setDisplayName(
                translated(getConfigurationLoader().RANK_ICON_DISPLAY_NAME
                        .replace("{prefix}", translated(rankPrefix))
                        .replace("{name}", translated(rankIdentifier))
                )
        );

        final String remainingCoins = (getInstance().getCurrency(viewer.getUniqueId()) < cost
                ? format(cost - getInstance().getCurrency(viewer.getUniqueId()))
                : "§a0 (utilize /rankup)"
        );

        itemMeta.setLore(
                getConfigurationLoader().LORE_RANK_ICON.stream()
                        .map(loreLine -> loreLine = translated(loreLine)
                                .replace("{cost}", format(cost))
                                .replace("{remainingCoins}", remainingCoins) //TODO: implement player money - cost
                                .replace("{upgrade}", "§aClique para upar de rank!")
                                .replace("{prefix}", translated(rankPrefix))
                                .replace("{name}", translated(rankIdentifier))
                        ).collect(Collectors.toList())
        );

        icon.setItemMeta(itemMeta);
        return icon;
    }

    protected String translated(String toTranslate) {
        return ChatColor.translateAlternateColorCodes('&', toTranslate);
    }

}


