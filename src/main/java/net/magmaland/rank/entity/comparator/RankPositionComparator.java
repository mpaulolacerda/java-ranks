package net.magmaland.rank.entity.comparator;

import net.magmaland.rank.entity.Rank;

import java.util.Comparator;

public class RankPositionComparator implements Comparator<Rank> {

    @Override
    public int compare(Rank rank1, Rank rank2) {
        return rank1.getPosition() - rank2.getPosition();
    }

}
