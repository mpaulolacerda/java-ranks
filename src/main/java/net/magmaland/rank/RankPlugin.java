package net.magmaland.rank;

import lombok.Getter;
import lombok.SneakyThrows;
import me.saiintbrisson.bukkit.command.BukkitFrame;
import me.saiintbrisson.db.Credentials;
import me.saiintbrisson.db.Database;
import me.saiintbrisson.minecraft.InventoryFrame;
import me.saiintbrisson.safe.Try;
import net.magmaland.rank.api.RankUserAPI;
import net.magmaland.rank.entity.Rank;
import net.magmaland.rank.entity.adapter.RankFileAdapter;
import net.magmaland.rank.entity.adapter.repository.RankUserAdapter;
import net.magmaland.rank.entity.cache.RankUserCache;
import net.magmaland.rank.entity.comparator.RankPositionComparator;
import net.magmaland.rank.entity.configuration.ConfigurationLoader;
import net.magmaland.rank.entity.controller.RankController;
import net.magmaland.rank.entity.queue.RankUserQueue;
import net.magmaland.rank.entity.repository.RankUserRepository;
import net.magmaland.rank.entity.user.RankUser;
import net.magmaland.rank.minecraft.commands.RankCommand;
import net.magmaland.rank.minecraft.commands.player.RankListCommand;
import net.magmaland.rank.minecraft.commands.player.RankupCommand;
import net.magmaland.rank.minecraft.inventories.RankInventoryView;
import net.magmaland.rank.minecraft.listeners.PlayerConnectionListener;
import org.bukkit.Bukkit;
import org.bukkit.plugin.java.JavaPlugin;

import java.io.File;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.io.OutputStream;
import java.sql.SQLException;

public class RankPlugin extends JavaPlugin {

    private static RankPlugin instance;
    private static ConfigurationLoader configurationLoader;

    private File rankFolder;
    private RankController rankController;
    private RankFileAdapter rankFileAdapter;

    @Getter
    private Database databaseProvider;

    @Getter
    private RankUserCache rankUserCache;

    @Override
    public void onLoad() {
        instance = this;
        saveDefaultConfig();
        loadConfiguration("sql.");

        setupDatabase();

        Try.of(() ->
            databaseProvider.getSqlLoader().loadFromResources(
                    getClass().getClassLoader(),
                    getClass().getProtectionDomain().getCodeSource().getLocation()
            )
        ).report("Carregando SQL").printStackTrace().expect(() -> setEnabled(false));
    }

    @Override
    public void onEnable() {
        databaseProvider.registerAdapter(RankUser.class, new RankUserAdapter());
        RankUserRepository rankUserRepository = new RankUserRepository(databaseProvider);
        databaseProvider.registerRepository(new RankUserRepository(databaseProvider));

        rankUserRepository.createTable();
        rankUserCache = new RankUserCache();

        new RankUserQueue().start();
        new RankUserAPI();

        loadConfiguration();

        rankFileAdapter = new RankFileAdapter();
        rankController = new RankController();

        loadRanks();
        loadCommands();
        new InventoryFrame(this).registerListener();
        Bukkit.getPluginManager().registerEvents(new PlayerConnectionListener(), this);
    }

    @Override
    public void onDisable() {
        RankUserQueue.getInstance().flush();
    }

    protected void loadConfiguration(String defaultSQLSection) {
        configurationLoader = ConfigurationLoader.builder()
                .SQL_HOST(loadString(defaultSQLSection + "host"))
                .SQL_DATABASE(loadString(defaultSQLSection + "database"))
                .SQL_PASSWORD(loadString(defaultSQLSection + "password"))
                .SQL_PORT(loadInt(defaultSQLSection + "port"))
                .SQL_USERNAME(loadString(defaultSQLSection + "user"))
                .LORE_RANK_ICON(getConfig().getStringList("ranks.icon-lore"))
                .RANK_ICON_DISPLAY_NAME(loadString("ranks.icon-name"))
                .build();
    }

    protected void loadCommands() {
        BukkitFrame bukkitFrame = new BukkitFrame(this);

        bukkitFrame.registerCommands(
                new RankListCommand(new RankInventoryView(this)),
                new RankCommand(),
                new RankupCommand()
        );
    }

    @SneakyThrows
    protected void loadRanks() {
        for (File file : rankFolder.listFiles()) {
            Rank rank = rankFileAdapter.convert(file);
            if (rank == null) continue;

            rankController.insertRank(rank);

            /**
             * Sorting ranks by position
             */
            rankController.getRanks().sort(new RankPositionComparator());
            System.out.println(rankController.getRanks());
        }

        if (rankController.empty()) {
            getLogger().severe("[AVISO] Nenhum rank foi carregado. Há algo de errado?");
            Bukkit.shutdown();
            return;
        }

        if (rankController.byPosition(0) == null) {
            getLogger().severe("[ERRO] O primeiro rank não foi encontrado!");
            Bukkit.shutdown();
        }

    }

    protected void loadConfiguration() {
        if (!getDataFolder().exists()) getDataFolder().mkdirs();

        getConfig().options().copyDefaults(false);
        saveDefaultConfig();

        rankFolder = new File(getDataFolder(), "ranks/");

        if (!rankFolder.exists()) {
            rankFolder.mkdirs();
            copyResource("stone_rank.yml", new File(rankFolder, "stone_rank.yml"));
        }
    }

    public static ConfigurationLoader getConfigurationLoader() {
        return configurationLoader;
    }

    public static RankPlugin getInstance() {
        return instance;
    }

    private void copyResource(String name, File to) {
        try {
            if (!to.exists())
                to.createNewFile();

            InputStream in = getResource(name);
            OutputStream out = new FileOutputStream(to);
            byte[] buf = new byte[1024];

            int len;
            while ((len = in.read(buf)) > 0) {
                out.write(buf, 0, len);
            }

            out.close();
            in.close();
        } catch (Exception exception) {
            exception.printStackTrace();
            Bukkit.shutdown();
        }
    }

    public RankController getRankController() {
        return rankController;
    }

    protected String loadString(String path) {
        return getConfig().getString(path);
    }

    protected int loadInt(String path) {
        return getConfig().getInt(path);
    }

    protected void setupDatabase() {
        Credentials credentials = new Credentials(
                configurationLoader.SQL_HOST,
                configurationLoader.SQL_PORT,
                configurationLoader.SQL_USERNAME,
                configurationLoader.SQL_PASSWORD,
                configurationLoader.SQL_DATABASE
        );

        databaseProvider = new Database("com.mysql.jdbc.Driver", credentials);

        Try.of(() -> databaseProvider.getSqlLoader().loadFromResources())
                .report("Carregando SQL")
                .printStackTrace()
                .expect(() -> setEnabled(false));

        try {
            databaseProvider.createConnection();
        } catch (SQLException | ClassNotFoundException throwables) {
            throwables.printStackTrace();
        }
    }

}
