package net.magmaland.rank.api;

import lombok.Getter;
import net.magmaland.economy.api.EconomyUserAPI;
import net.magmaland.rank.RankPlugin;
import net.magmaland.rank.entity.Rank;
import net.magmaland.rank.entity.cache.RankUserCache;
import net.magmaland.rank.entity.error.RankResponse;
import net.magmaland.rank.entity.error.exceptions.InsufficientMoneyException;
import net.magmaland.rank.entity.error.exceptions.InvalidPlayerException;
import net.magmaland.rank.entity.error.exceptions.LastRankException;
import net.magmaland.rank.entity.queue.RankUserQueue;
import net.magmaland.rank.entity.user.RankUser;
import net.magmaland.rank.entity.wrapper.types.RankUpgradeWrapper;
import org.bukkit.entity.Player;

public class RankUserAPI {

    @Getter private static RankUserAPI instance;

    private final RankPlugin rankPlugin = RankPlugin.getInstance();
    private final RankUserCache rankUserCache = rankPlugin.getRankUserCache();
    private final EconomyUserAPI economyUserAPI = EconomyUserAPI.getInstance();

    public RankUserAPI() {
        instance = this;
    }

    public RankUser getRankUser(Player player) throws InvalidPlayerException {
        final RankUser rankUser = rankUserCache.get(player.getUniqueId());
        if (rankUser == null) throw new InvalidPlayerException();

        return rankUser;
    }

    public RankResponse upgradeRank(Player target) throws InvalidPlayerException, LastRankException, InsufficientMoneyException {
        final RankUser rankUser = rankUserCache.get(target.getUniqueId());
        if (rankUser == null) throw new InvalidPlayerException();

        final Rank currentRank = rankUser.getCurrentRank();
        if (currentRank == null) return RankResponse.FAIL;

        Rank nextRank = rankPlugin.getRankController().byPosition(currentRank.getPosition() + 1);

        if (economyUserAPI.getCurrency(target.getUniqueId()) < nextRank.getCost())
            throw new InsufficientMoneyException(nextRank.getCost());

        economyUserAPI.removeCurrency(target.getUniqueId(), nextRank.getCost());
        RankUpgradeWrapper.whenUpgrade(target, currentRank, nextRank);

        rankUser.setCurrentRank(nextRank);
        RankUserQueue.getInstance().insertQueue(rankUser);
        return RankResponse.SUCCESS;
    }

}
