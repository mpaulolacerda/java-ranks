CREATE TABLE IF NOT EXISTS rank_user(
    uuid CHAR(36) PRIMARY KEY,
    curr_rank_id INT NOT NULL DEFAULT 0,
    UNIQUE(uuid)
);