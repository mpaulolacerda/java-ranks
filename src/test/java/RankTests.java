import net.magmaland.motherboard.formatter.CoinsFormatter;
import net.magmaland.rank.entity.Rank;
import net.magmaland.rank.entity.comparator.RankPositionComparator;

import java.util.*;

public class RankTests {

    public static void main(String[] args) {
        List<Rank> ranks = new ArrayList<>();

        Rank rank1 = Rank.builder()
                .rankId("teste")
                .rankIdentifier("teste11")
                .cost(100.0)
                .position(0)
                .build();

        Rank rank2 = Rank.builder()
                .rankId("teste2")
                .rankIdentifier("teste22")
                .cost(200.0)
                .position(2)
                .build();

        Rank rank3 = Rank.builder()
                .rankId("teste3")
                .rankIdentifier("tete33")
                .cost(300.0)
                .position(1)
                .build();

        ranks.add(rank1);
        ranks.add(rank2);
        ranks.add(rank3);

        Collections.sort(ranks, new RankPositionComparator());

        System.out.println(ranks);
    }

}
